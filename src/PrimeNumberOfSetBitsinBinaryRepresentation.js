/*https://leetcode.com/problems/prime-number-of-set-bits-in-binary-representation*/
/**
 * @param {number} L
 * @param {number} R
 * @return {number}
 */
var countPrimeSetBits = function(L, R) {
    let listPrime = [ 2, 3, 5, 7, 11, 13, 17, 19];
    let count = 0;
    for(i=L;i<=R;i++)
    {
        let numList = i.toString(2).split("");
        let count1= 0
        for(j=0;j< numList.length;j++)
        {
            if(numList[j] ==1) count1++;
            
        }
        if(listPrime.indexOf(count1) != -1) count ++;
    }
    return count;  
};
