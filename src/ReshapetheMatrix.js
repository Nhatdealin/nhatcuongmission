/*https://leetcode.com/problems/reshape-the-matrix/submissions*/
/**
 * @param {number[][]} nums
 * @param {number} r
 * @param {number} c
 * @return {number[][]}
 */
var matrixReshape = function(nums, r, c) {
    let list = [];
    let result= [];
    for(i=0;i<r;i++)
    {
        result[i] = [c];
    } 
    if(r*c != nums.length*nums[0].length) return nums;
    for(i=nums.length-1;i>=0;i--)
        for(j=nums[0].length-1;j>=0;j--){
            list.push(nums[i][j]);
        }
     for(i=0;i<r;i++)
        for(j=0;j<c;j++){ 
            result[i][j]= list.pop();
        }
    return result;
};
