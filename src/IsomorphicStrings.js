/*https://leetcode.com/problems/isomorphic-strings*/
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function(s, t) {
    let arr1=s.split("");
    let arr2 = t.split("");
    if(arr1.length != arr2.length) return false;
    for(i=0;i<arr1.length;i++)
    {
        for(j=i+1;j<arr2.length;j++)
        {
             if((arr2[i] == arr2[j] && arr1[j] != arr1[i]) || (arr1[i] == arr1[j] && arr2[j] != arr2[i]))
                return false;
        }
           
    }
    return true;
        
};
