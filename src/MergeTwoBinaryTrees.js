//DoMinhNhat
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} t1
 * @param {TreeNode} t2
 * @return {TreeNode}
 */
var mergeTrees = function(t1, t2) {
    let t3=t1;
    if(t1 == null ) 
        if(t2 == null) 
            {
                return null ;
            }
        else 
            {
             t3 = new TreeNode(t2.val);
            t3.left = mergeTrees(t3.left,t2.left);
            t3.right = mergeTrees(t3.right,t2.right); 
            }
    else
            if(t2 !=null)
        {
            t3.val = t1.val + t2.val;
            t3.left = mergeTrees(t3.left,t2.left);
            t3.right = mergeTrees(t3.right,t2.right); 
            
            }
        
    return t3;

};
