//DoMinhNhat
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */

var binaryTreePaths = function(root) {
    var path = [];
    // return recur(root, path);
    
    if(root == null)  return [];
    {
        if(root.left == null && root.right == null)
            {
                if(path.length == 0) return ["" +root.val];
                else return root.val;
            }
        else 
            {
                if(root.left != null) 
                    {
                        binaryTreePaths(root.left).forEach(function (left){
                            path.push(root.val +"->" + left);
                        });
                        
                    }
                if(root.right != null)
                    {
                        binaryTreePaths(root.right).forEach(function (right){
                            path.push(root.val +"->" + right);
                        });
                        
                    }
            }  
    }
    return path;
};
