/*https://leetcode.com/problems/1-bit-and-2-bit-characters*/
/**
 * @param {number[]} bits
 * @return {boolean}
 */
var isOneBitCharacter = function(bits) {

        bits.pop();
        for(i=0;i<bits.length;i+=2)
       {
             if(bits[i] == 1 ) 
                 {
                     if(bits[i+1] == null) return false;
                 }
           else i--;
       }
    return true;  

       
};
