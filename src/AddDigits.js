/*DoMinhNhat*/
/**
 * @param {number} num
 * @return {number}
 */
var addDigits = function(num) {
    if(num<10) return num;
    let string =num.toString();
    let arr = string.split("");
    let sum = arr.reduce(function (sum, current) {
        return sum + parseInt(current);
    }, 0);
    return addDigits(sum);
};
