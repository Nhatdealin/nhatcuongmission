/*https://leetcode.com/problems/palindrome-number*/
/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
    let stringNumber =x.toString();
    let stringReverse = stringNumber.split("").reverse().join("");
    if(stringNumber == stringReverse) return true;
    else return false;
};
