/*https://leetcode.com/problems/pascals-triangle-ii*/
/**
 * @param {number} rowIndex
 * @return {number[]}
 */
var getRow = function(rowIndex) {
    let result = [];
    if(rowIndex == 0) return [1];
    if(rowIndex == 1) return [1,1];
    let tmp = getRow(rowIndex -1);
    result[0]=1;
    result[rowIndex]=1;
    for(i=0;i< rowIndex -1 ;i++)
        {      
            result[i+1] = tmp[i] +tmp[i+1];
        }
    return result;
};
