/*https://leetcode.com/problems/longest-uncommon-subsequence-i*/
/**
 * @param {string} a
 * @param {string} b
 * @return {number}
 */
var findLUSlength = function(a, b) {
    if(a.length > b.length) 
    {
             for(i=a.length;i>=0;i--)
        {

            if(b.indexOf(a.substr(0,i)) == -1) return i;
        }
    }
    else
    {
             for(i=b.length;i>=0;i--)
        {

            if(a.indexOf(b.substr(0,i)) == -1) return i;
        }
    }
    
   
     return -1
};
