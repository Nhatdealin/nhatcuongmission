/*https://leetcode.com/problems/range-addition-ii/submissions*/
/**
 * @param {number} m
 * @param {number} n
 * @param {number[][]} ops
 * @return {number}
 */
var maxCount = function(m, n, ops) {
    let arr1 = [m];
    if(ops.length == 0) return m*n;
    let minrow = ops[0][0];
    let mincol =ops[0][1];
    
    for(i=0;i< ops.length ; i++){
        if(minrow > ops[i][0]) minrow = ops[i][0];
         if(mincol > ops[i][1]) mincol = ops[i][1];
    }
        

    return minrow*mincol;
    
};
