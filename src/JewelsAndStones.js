//DoMinhNhat
/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
var numJewelsInStones = function(J, S) {
    let stringslice = S.slice("");
    let count=0;
    for(i=0;i< stringslice.length;i++)
        {
            if(J.includes(stringslice[i])) count++;
        }
    return count;
};
