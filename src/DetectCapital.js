/*https://leetcode.com/problems/detect-capital/*/
/**
 * @param {string} word
 * @return {boolean}
 */
var detectCapitalUse = function(word) {
    
    if(word[0].charCodeAt(0) >= 97 && word[0].charCodeAt(0) <= 122)
    {
        
        for(i =1;i< word.length;i++) if(word[i].charCodeAt(0) < 97 || word[i].charCodeAt(0) > 122) return false;
        return true;
    }
    if(word[0].charCodeAt(0) >= 65 && word[0].charCodeAt(0) <= 90)
    {
        for(i =1;i< word.length;i++) if(word[i].charCodeAt(0) < 65 || word[i].charCodeAt(0) > 90) 
           for(i =1;i< word.length;i++) if(word[i].charCodeAt(0) < 97 || word[i].charCodeAt(0) > 122) return false;
        return true;
    }
    
};
