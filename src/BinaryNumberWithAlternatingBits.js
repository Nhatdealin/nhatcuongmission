/*https://leetcode.com/problems/binary-number-with-alternating-bits*/
/**
 * @param {number} n
 * @return {boolean}
 */
var hasAlternatingBits = function(n) {
    let numList = n.toString(2).split("");
    for(i=0;i<numList.length-1;i++)
    {
        if(numList[i] == numList[i+1]) return false;
    }
    return true;
};
