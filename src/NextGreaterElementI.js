/*https://leetcode.com/problems/next-greater-element-i*/
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var nextGreaterElement = function(nums1, nums2) {
    for(i=0;i<nums1.length;i++)
    {
        let isReplace = false;
        for(k=nums2.indexOf(nums1[i]);k< nums2.length;k++ )
        {
            if(nums1[i] < nums2[k]) 
            {
                isReplace = true;
                nums1[i] = nums2[k];
                break;
            }
        }
        if(!isReplace) nums1[i] = -1;
    }
    return nums1;
};
