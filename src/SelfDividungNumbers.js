/*https://leetcode.com/problems/self-dividing-numbers*/
/**
 * @param {number} left
 * @param {number} right
 * @return {number[]}
 */

var selfDividingNumbers = function(left, right) {
    let arr = [];
 var selfDividing = function(a) {     
       let b =  a.toString().split("");
        for(i=0;i< b.length;i++)
            {
                if(a % b[i] !=0 ) return false;
            }
        return true;
    }
    for(h =left;h <=right;h++)
        {
            if(selfDividing(h)) arr.push(h);
        }
    return arr;
};
