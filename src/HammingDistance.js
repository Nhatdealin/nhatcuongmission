//DoMinhNhat
/**
 * @param {number} x
 * @param {number} y
 * @return {number}
 */
var hammingDistance = function(x, y) {
    let arr1 = x.toString(2).split("");
    let arr2 = y.toString(2).split("");
    let size = Math.abs(arr1.length - arr2.length)
    let count = 0;
    if (arr1.length > arr2.length) 
        { 
            for(i=0;i<size;i++ ) 
                arr2.unshift("0");    
        }
    else
        { 
            for(i=0;i< size;i++ )
                   arr1.unshift("0");      
        }
    for(i=0;i<arr1.length;i++ )
        if(arr1[i] != arr2[i]) count ++;
    
    return count
};
