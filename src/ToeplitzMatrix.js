/*https://leetcode.com/problems/toeplitz-matrix*/
/**
 * @param {number[][]} matrix
 * @return {boolean}
 */
var isToeplitzMatrix = function(matrix) {
  let map = {}; 
  
    for (let c = 0; c < matrix[0].length ; c++) {
            map[0-c] = matrix[0][c]; 
        for (let r = 1; r < matrix.length; r++)  {map[r] = matrix[r][0];
            }
  }
    for (let r = 1; r < matrix.length; r++)  {
        for (let c = 1; c < matrix[0].length ; c++){
             if (map[r-c] != matrix[r][c])  return false;
    }
    }
  return true
};
