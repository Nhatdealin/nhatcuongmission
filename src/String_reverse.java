//https://leetcode.com/problems/reverse-string/submissions// 
class Solution {
    public String reverseString(String s) {
    
        return new StringBuffer(s).reverse().toString();}
    
}
