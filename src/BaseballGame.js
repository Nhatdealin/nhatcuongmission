/*https://leetcode.com/problems/baseball-game/submissions*/
/**
 * @param {string[]} ops
 * @return {number}
 */
var calPoints = function(ops) {
    let point =[];
    
    for(i=0;i< ops.length ;i++)
        {
            if(ops[i].toUpperCase() == "C") {
             
               point.pop();
            }
            else
            if(ops[i].toUpperCase() == "D") 
                {
                    
                    point.push(point[point.length-1]*2);
                }
            else 
                if(ops[i] == "+") point.push(Number(point[point.length-1])+ Number(point[point.length-2]));
            else 
                {
                  point.push(ops[i]);                
                }
        }
       sum= point.reduce(add, 0);
    function add(a, b) {
    return Number(a) + Number(b);
}
    return sum;
};
