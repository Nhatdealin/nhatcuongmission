/*https://leetcode.com/problems/balanced-binary-tree*/
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isBalanced = function(root) {
    
function checkHeight(root)
{
    if(root == null)
        return 0;
    let left = checkHeight(root.left);
    let right = checkHeight(root.right);
    if(left<0 || right<0)
        return -1;
    if(Math.abs(left-right)>=2)
        return -1;
    return Math.max(left,right)+1;
}
    
    return checkHeight(root)>=0;
    
};
