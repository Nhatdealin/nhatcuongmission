/*https://leetcode.com/problems/reverse-vowels-of-a-string*/
/**
 * @param {string} s
 * @return {string}
 */
var reverseVowels = function(s) {
    let arrVowels = ["u","e","o","a","i"]
    let arr1 = s.split("");
    let arr2 = [];
    let arr3 = [];
    let k = "";
    for(i = arr1.length -1; i>= 0  ; i--)
        {
            if(arrVowels.indexOf(arr1[i].toLowerCase()) != -1) 
                {
                    arr2.push(arr1[i]);
                    arr3.unshift(i);
                }
        }
    for(i=0;i< arr2.length;i++)
        {
            arr1[arr3[i]] = arr2[i];
        }
    for(i=0;i< arr1.length;i++) k+=arr1[i];
           
    return k;
    
};
